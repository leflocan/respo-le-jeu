from flask import Flask, render_template, jsonify
import csv
import random

app = Flask(__name__)

# Read the questions from the CSV file
questions = []
# Open the CSV file and read the data into a list of dictionaries
with open('questions.csv') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    questions = [row for row in csv_reader]

# Define the routes
@app.route("/")
def index():
    return render_template("index.html")

@app.route("/questions")
def get_questions():
    # Shuffle the list of questions
    random.shuffle(questions)

    # Assign new IDs to each question
    for index, question in enumerate(questions):
        question["id"] = index + 1
    return jsonify(questions)

# Run the app
if __name__ == "__main__":
    app.run()